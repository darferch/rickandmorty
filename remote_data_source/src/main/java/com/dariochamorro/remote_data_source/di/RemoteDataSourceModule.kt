package com.dariochamorro.remote_data_source.di

import com.dariochamorro.data.datasources.CharacterRemoteDataSource
import com.dariochamorro.data.datasources.EpisodeRemoteDataSource
import com.dariochamorro.remote_data_source.CharacterRemoteDataSourceImpl
import com.dariochamorro.remote_data_source.EpisodeRemoteDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RemoteDataSourceModule {

    @Binds
    @Singleton
    abstract fun bindCharacterRemoteDataSource(characterRemoteDataSourceImpl: CharacterRemoteDataSourceImpl): CharacterRemoteDataSource

    @Binds
    @Singleton
    abstract fun bindEpisodeRemoteDataSource(episodeRemoteDataSourceImpl: EpisodeRemoteDataSourceImpl): EpisodeRemoteDataSource
}
