package com.dariochamorro.remote_data_source.di

import com.dariochamorro.remote_data_source.apis.CharacterApi
import com.dariochamorro.remote_data_source.apis.EpisodeApi
import com.dariochamorro.remote_data_source.apis.NetworkProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideCharacterApi(provider: NetworkProvider): CharacterApi =
        provider.create(CharacterApi::class.java)

    @Provides
    @Singleton
    fun provideEpisodeApi(provider: NetworkProvider): EpisodeApi = provider.create(EpisodeApi::class.java)
}
