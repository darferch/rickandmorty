package com.dariochamorro.remote_data_source.dto

data class ListResponseDto<T>(
    val results: List<T>
)
