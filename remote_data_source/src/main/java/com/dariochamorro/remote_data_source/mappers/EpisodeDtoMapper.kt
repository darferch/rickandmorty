package com.dariochamorro.remote_data_source.mappers

import com.dariochamorro.domain.common.ModelMapper
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.remote_data_source.dto.EpisodeDto
import javax.inject.Inject

class EpisodeDtoMapper @Inject constructor() : ModelMapper<Episode, EpisodeDto>() {
    override fun toModel(data: EpisodeDto): Episode = Episode(
        id = data.id,
        name = data.name,
        airDate = data.air_date,
        episode = data.episode,
        characters = data.characters.map {
            it.split("/").last().toInt()
        }
    )
}
