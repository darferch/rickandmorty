package com.dariochamorro.remote_data_source.mappers

import com.dariochamorro.domain.common.ModelMapper
import com.dariochamorro.domain.models.Character
import com.dariochamorro.domain.models.CharacterStatus
import com.dariochamorro.remote_data_source.dto.CharacterDto
import javax.inject.Inject

class CharacterDtoMapper @Inject constructor() : ModelMapper<Character, CharacterDto>() {
    override fun toModel(data: CharacterDto): Character = Character(
        id = data.id,
        name = data.name,
        image = data.image,
        status = when (data.status) {
            "Alive" -> CharacterStatus.Alive
            "Dead" -> CharacterStatus.Dead
            else -> CharacterStatus.Unknown
        },
        species = data.species
    )
}
