package com.dariochamorro.remote_data_source.util

object Urls {
    const val BASE_URL = "https://rickandmortyapi.com/api/"
    const val EPISODE = "episode"
    const val CHARACTERS_BY_IDS = "character/{ids}"
}
