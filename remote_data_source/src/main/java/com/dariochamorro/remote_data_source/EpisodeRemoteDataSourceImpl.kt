package com.dariochamorro.remote_data_source

import com.dariochamorro.data.datasources.EpisodeRemoteDataSource
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.remote_data_source.apis.EpisodeApi
import com.dariochamorro.remote_data_source.mappers.EpisodeDtoMapper
import javax.inject.Inject

class EpisodeRemoteDataSourceImpl @Inject constructor(
    private val api: EpisodeApi,
    private val mapper: EpisodeDtoMapper
) : EpisodeRemoteDataSource {

    override suspend fun getAllEpisodes(): List<Episode> {
        val data = api.getEpisodes()
        return mapper.toListModel(data.results)
    }
}
