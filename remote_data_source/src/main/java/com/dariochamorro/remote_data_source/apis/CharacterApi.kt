package com.dariochamorro.remote_data_source.apis

import com.dariochamorro.remote_data_source.dto.CharacterDto
import com.dariochamorro.remote_data_source.util.Urls
import retrofit2.http.GET
import retrofit2.http.Path

interface CharacterApi {

    @GET(Urls.CHARACTERS_BY_IDS)
    suspend fun getCharactersById(@Path("ids") ids: String): List<CharacterDto>
}
