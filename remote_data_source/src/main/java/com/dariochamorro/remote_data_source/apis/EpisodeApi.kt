package com.dariochamorro.remote_data_source.apis

import com.dariochamorro.remote_data_source.dto.EpisodeDto
import com.dariochamorro.remote_data_source.dto.ListResponseDto
import com.dariochamorro.remote_data_source.util.Urls
import retrofit2.http.GET

interface EpisodeApi {

    @GET(Urls.EPISODE)
    suspend fun getEpisodes(): ListResponseDto<EpisodeDto>
}
