package com.dariochamorro.remote_data_source

import com.dariochamorro.data.datasources.CharacterRemoteDataSource
import com.dariochamorro.domain.models.Character
import com.dariochamorro.remote_data_source.apis.CharacterApi
import com.dariochamorro.remote_data_source.mappers.CharacterDtoMapper
import javax.inject.Inject

class CharacterRemoteDataSourceImpl @Inject constructor(
    private val api: CharacterApi,
    private val mapper: CharacterDtoMapper
) : CharacterRemoteDataSource {

    override suspend fun getCharactersByIds(ids: List<Int>): List<Character> {
        val data = api.getCharactersById(ids.joinToString(","))
        return mapper.toListModel(data)
    }
}
