package com.dariochamorro.domain

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.AsyncResultStatus
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.respositories.EpisodeRepository
import com.dariochamorro.domain.usecases.LoadEpisodesUseCase
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class LoadEpisodesUseCaseTest : CoroutineUnitTest() {

    @Mock
    lateinit var episodeRepository: EpisodeRepository
    lateinit var loadEpisodesUseCase: LoadEpisodesUseCase

    @Before
    fun setup() {
        loadEpisodesUseCase = LoadEpisodesUseCase(episodeRepository, testDispatchers)
    }

    @Test
    fun `should return a success when is invoked`() = runBlockingTest {
        // Given
        whenever(episodeRepository.loadEpisodes()).thenReturn(AsyncResult.success(data = Unit))

        // When
        val result = loadEpisodesUseCase()

        // Then
        Assert.assertEquals(AsyncResultStatus.SUCCESS, result.status)
    }

    @Test
    fun `should return a AsyncResult with error if repository fail`() = runBlockingTest {
        // Given
        whenever(episodeRepository.loadEpisodes()).thenReturn(
            AsyncResult.error(Errors.NetworkError)
        )

        // When
        val result = loadEpisodesUseCase()

        // Then
        Assert.assertEquals(AsyncResultStatus.ERROR, result.status)
        Assert.assertEquals(Errors.NetworkError, result.error)
    }
}
