package com.dariochamorro.domain

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.domain.respositories.EpisodeRepository
import com.dariochamorro.domain.usecases.ListEpisodesUseCase
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class ListEpisodesUseCaseTest : CoroutineUnitTest() {

    @Mock
    lateinit var repository: EpisodeRepository
    lateinit var listEpisodesUseCase: ListEpisodesUseCase

    @Before
    fun setup() {
        listEpisodesUseCase = ListEpisodesUseCase(repository, testDispatchers)
    }

    @Test
    fun `should emit a flow of a list of episodes when it is invoked`() = runBlockingTest {
        // Given
        val data = SampleFactory.listSample(Episode::class, 2)
        val asyncData = AsyncResult.success(data)
        whenever(repository.listEpisodes()).thenReturn(listOf(asyncData).asFlow())

        // When
        var result: AsyncResult<List<Episode>>? = null
        listEpisodesUseCase().collect { result = it }

        // Then
        assertEquals(data, result)
    }
}
