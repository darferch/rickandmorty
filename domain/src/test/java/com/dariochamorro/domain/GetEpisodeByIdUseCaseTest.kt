package com.dariochamorro.domain

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.domain.respositories.EpisodeRepository
import com.dariochamorro.domain.usecases.GetEpisodeByIdUseCase
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class GetEpisodeByIdUseCaseTest : CoroutineUnitTest() {

    @Mock
    lateinit var repository: EpisodeRepository
    lateinit var getEpisodeByIdUseCase: GetEpisodeByIdUseCase

    @Before
    fun setup() {
        getEpisodeByIdUseCase = GetEpisodeByIdUseCase(repository, testDispatchers)
    }

    @Test
    fun `should return an Episode by id when it is invoked`() = runBlockingTest {
        // Given
        val data = SampleFactory.singleSample(Episode::class)
        val asyncData = AsyncResult.success(data)
        whenever(repository.getEpisodeById(data.id)).thenReturn(asyncData)

        // When
        val result = getEpisodeByIdUseCase(data.id)

        // Then
        Assert.assertEquals(asyncData, result)
    }
}
