package com.dariochamorro.domain

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.AsyncResultStatus
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.models.Character
import com.dariochamorro.domain.respositories.CharacterRepository
import com.dariochamorro.domain.usecases.GetCharactersByIdsUseCase
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class GetCharactersByIdsUseCaseTest : CoroutineUnitTest() {

    @Mock
    lateinit var characterRepository: CharacterRepository
    lateinit var getCharactersByIdsUseCase: GetCharactersByIdsUseCase

    @Before
    fun setup() {
        getCharactersByIdsUseCase = GetCharactersByIdsUseCase(characterRepository, testDispatchers)
    }

    @Test
    fun `should return a list of characters when is invoked`() = runBlockingTest {
        // Given
        val data = SampleFactory.listSample(Character::class, 2)
        val ids = listOf(1, 2)
        whenever(characterRepository.characterByIds(ids)).thenReturn(AsyncResult.success(data))

        // When
        val result = getCharactersByIdsUseCase(ids)

        // Then
        assertEquals(AsyncResultStatus.SUCCESS, result.status)
        assertEquals(data, result.data)
    }

    @Test
    fun `should return a AsynResult with error if repository fail`() = runBlockingTest {
        // Given
        val ids = listOf(1, 2)
        whenever(characterRepository.characterByIds(any())).thenReturn(
            AsyncResult.error(Errors.NetworkError)
        )

        // When
        val result = getCharactersByIdsUseCase(ids)

        // Then
        assertEquals(AsyncResultStatus.ERROR, result.status)
        assertEquals(Errors.NetworkError, result.error)
    }
}
