package com.dariochamorro.domain.models

data class Episode(
    val id: Int,
    val name: String,
    val airDate: String,
    val episode: String,
    val characters: List<Int>
)
