package com.dariochamorro.domain.models

enum class CharacterStatus { Alive, Dead, Unknown }

data class Character(
    val id: Int,
    val name: String,
    val status: CharacterStatus,
    val species: String,
    val image: String
)
