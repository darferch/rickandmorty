package com.dariochamorro.domain.usecases

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.domain.respositories.EpisodeRepository
import com.dariochamorro.domain.util.DispatcherProvider
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetEpisodeByIdUseCase @Inject constructor(
    private val repository: EpisodeRepository,
    private val dispatchers: DispatcherProvider
) {

    suspend operator fun invoke(episodeId: Int): AsyncResult<Episode> =
        withContext(dispatchers.io()) {
            repository.getEpisodeById(episodeId)
        }
}
