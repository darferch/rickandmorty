package com.dariochamorro.domain.usecases

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Character
import com.dariochamorro.domain.respositories.CharacterRepository
import com.dariochamorro.domain.util.DispatcherProvider
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetCharactersByIdsUseCase @Inject constructor(
    private val repository: CharacterRepository,
    private val dispatchers: DispatcherProvider
) {

    suspend operator fun invoke(ids: List<Int>): AsyncResult<List<Character>> =
        withContext(dispatchers.io()) {
            repository.characterByIds(ids)
        }
}
