package com.dariochamorro.domain.usecases

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.domain.respositories.EpisodeRepository
import com.dariochamorro.domain.util.DispatcherProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ListEpisodesUseCase @Inject constructor(
    private val repository: EpisodeRepository,
    private val dispatchers: DispatcherProvider
) {

    operator fun invoke(): Flow<AsyncResult<List<Episode>>> {
        return repository.listEpisodes()
            .flowOn(dispatchers.io())
    }
}
