package com.dariochamorro.domain.respositories

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Character

interface CharacterRepository {

    suspend fun characterByIds(ids: List<Int>): AsyncResult<List<Character>>
}
