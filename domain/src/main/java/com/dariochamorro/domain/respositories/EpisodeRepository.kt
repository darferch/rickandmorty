package com.dariochamorro.domain.respositories

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Episode
import kotlinx.coroutines.flow.Flow

interface EpisodeRepository {

    fun listEpisodes(): Flow<AsyncResult<List<Episode>>>

    suspend fun loadEpisodes(): AsyncResult<Unit>

    suspend fun getEpisodeById(episodeId: Int): AsyncResult<Episode>
}
