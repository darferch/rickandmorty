package com.dariochamorro.presentation

import com.dariochamorro.android_testing_common.LiveDataUnitTest
import com.dariochamorro.android_testing_common.util.getOrAwaitValue
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.domain.usecases.ListEpisodesUseCase
import com.dariochamorro.domain.usecases.LoadEpisodesUseCase
import com.dariochamorro.presentation.episodes.EpisodesViewModel
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class EpisodesViewModelTest : LiveDataUnitTest() {

    @Mock
    lateinit var listEpisodesUseCase: ListEpisodesUseCase

    @Mock
    lateinit var loadEpisodesUseCase: LoadEpisodesUseCase
    lateinit var episodesViewModel: EpisodesViewModel

    @Before
    fun setup() {
        episodesViewModel =
            EpisodesViewModel(listEpisodesUseCase, loadEpisodesUseCase, testDispatchers)
    }

    @Test
    fun `should emit a list of episodes through a LiveData`() {
        // Given
        val data = SampleFactory.listSample(Episode::class, 2)
        val asyncData = AsyncResult.success(data)
        whenever(listEpisodesUseCase()).thenReturn(listOf(asyncData).asFlow())

        // When
        val result: AsyncResult<List<Episode>> = episodesViewModel.episodes.getOrAwaitValue()

        // Then
        assertEquals(asyncData, result)
    }

    @Test
    fun `loadEpisodes should emit AsyncResult with loading status through a liveData`() {
        // Given
        val expectedResult = AsyncResult.loading<List<Episode>>()

        // When
        val result: AsyncResult<Unit> = episodesViewModel.loadEpisodeResult.getOrAwaitValue {
            episodesViewModel.loadEpisodes()
        }

        // Then
        assertEquals(expectedResult, result)
    }

    @Test
    fun `loadEpisodes should emit AsyncResult with success status through a liveData`() =
        runBlockingTest {
            // Given
            val expectedResult = AsyncResult.success(Unit)

            whenever(loadEpisodesUseCase()).thenReturn(expectedResult)

            // When
            episodesViewModel.loadEpisodes()
            val result: AsyncResult<Unit> = episodesViewModel.loadEpisodeResult.getOrAwaitValue()

            // Then
            assertEquals(expectedResult, result)
        }
}
