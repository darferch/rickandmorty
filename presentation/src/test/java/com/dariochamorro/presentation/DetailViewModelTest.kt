package com.dariochamorro.presentation

import com.dariochamorro.android_testing_common.LiveDataUnitTest
import com.dariochamorro.android_testing_common.util.getOrAwaitValue
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Character
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.domain.usecases.GetCharactersByIdsUseCase
import com.dariochamorro.domain.usecases.GetEpisodeByIdUseCase
import com.dariochamorro.presentation.detail.DetailViewModel
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class DetailViewModelTest : LiveDataUnitTest() {

    @Mock
    lateinit var getCharactersByIdsUseCase: GetCharactersByIdsUseCase

    @Mock
    lateinit var getEpisodeByIdUseCase: GetEpisodeByIdUseCase
    lateinit var detailViewModel: DetailViewModel

    @Before
    fun setup() {
        detailViewModel = DetailViewModel(
            getCharactersByIdsUseCase,
            getEpisodeByIdUseCase,
            testDispatchers
        )
    }

    @Test
    fun `getCharacters should emit AsyncResult with loading status through a liveData`() =
        runBlockingTest {
            // Given
            val characterIds = listOf(1, 2, 3)
            val expectedResult = AsyncResult.loading<List<Character>>()

            // When
            val result: AsyncResult<List<Character>> = detailViewModel.characters.getOrAwaitValue(
                afterObserve = {
                    detailViewModel.getCharacters(characterIds)
                }
            )

            // Then
            assertEquals(expectedResult, result)
        }

    @Test
    fun `getCharacters should emit AsyncResult with data status through a liveData`() =
        runBlockingTest {
            // Given
            val characterIds = listOf(1, 2, 3)
            val data = SampleFactory.listSample(Character::class, 3)
            val expectedResult = AsyncResult.success(data)

            whenever(getCharactersByIdsUseCase(characterIds)).thenReturn(expectedResult)

            // When
            detailViewModel.getCharacters(characterIds)
            val result: AsyncResult<List<Character>> = detailViewModel.characters.getOrAwaitValue()

            // Then
            assertEquals(expectedResult, result)
        }

    @Test
    fun `loadEpisode should emit AsyncResult with loading status through a liveData`() {
        // Given
        val episodeId = 1
        val expectedResult = AsyncResult.loading<Episode>()

        // When
        val result: AsyncResult<Episode> = detailViewModel.episode.getOrAwaitValue {
            detailViewModel.loadEpisode(episodeId)
        }

        // Then
        assertEquals(expectedResult, result)
    }

    @Test
    fun `loadEpisode should emit AsyncResult with data status through a liveData`() =
        runBlockingTest {
            // Given
            val episodeId = 1
            val data = SampleFactory.singleSample(Episode::class)
            val expectedResult = AsyncResult.success(data)

            whenever(getEpisodeByIdUseCase(episodeId)).thenReturn(expectedResult)

            // When
            detailViewModel.loadEpisode(episodeId)
            val result: AsyncResult<Episode> = detailViewModel.episode.getOrAwaitValue()

            // Then
            assertEquals(expectedResult, result)
        }
}
