package com.dariochamorro.presentation.common

import com.dariochamorro.domain.common.Errors

interface ErrorMessage {
    fun getMessage(error: Errors?): String
}
