package com.dariochamorro.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Character
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.domain.usecases.GetCharactersByIdsUseCase
import com.dariochamorro.domain.usecases.GetEpisodeByIdUseCase
import com.dariochamorro.domain.util.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val getCharactersByIdsUseCase: GetCharactersByIdsUseCase,
    private val getEpisodeByIdUseCase: GetEpisodeByIdUseCase,
    private val dispatchers: DispatcherProvider
) : ViewModel() {

    private val charactersControl: MutableLiveData<AsyncResult<List<Character>>> = MutableLiveData()
    val characters: LiveData<AsyncResult<List<Character>>> = charactersControl

    private val episodeControl: MutableLiveData<AsyncResult<Episode>> = MutableLiveData()
    val episode: LiveData<AsyncResult<Episode>> = episodeControl

    fun getCharacters(ids: List<Int>) = viewModelScope.launch(dispatchers.main()) {
        charactersControl.value = AsyncResult.loading()
        charactersControl.value = getCharactersByIdsUseCase(ids)
    }

    fun loadEpisode(episodeId: Int) = viewModelScope.launch(dispatchers.main()) {
        episodeControl.value = AsyncResult.loading()
        episodeControl.value = getEpisodeByIdUseCase(episodeId)
    }
}
