package com.dariochamorro.presentation.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.dariochamorro.domain.common.AsyncResultStatus.ERROR
import com.dariochamorro.domain.common.AsyncResultStatus.LOADING
import com.dariochamorro.domain.common.AsyncResultStatus.SUCCESS
import com.dariochamorro.domain.models.Character
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.presentation.R
import com.dariochamorro.presentation.common.ErrorMessage
import com.dariochamorro.presentation.databinding.FragmentDetailBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailFragment : Fragment() {

    @Inject
    lateinit var errorMessage: ErrorMessage
    private val args: DetailFragmentArgs by navArgs()
    private val viewModel: DetailViewModel by viewModels()
    private val adapter: CharacterAdapter = CharacterAdapter()

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!

    private var episode: Episode? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        loadEpisode()
        listenEpisodeData()
        listenCharactersData()
    }

    private fun setupRecycler() {
        binding.recyclerCharacters.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        binding.recyclerCharacters.adapter = adapter
    }

    private fun loadEpisode() {
        hideError()
        viewModel.loadEpisode(args.episodeId)
    }

    private fun listenEpisodeData() {
        viewModel.episode.observe(viewLifecycleOwner) {
            when (it.status) {
                LOADING -> showLoader()
                SUCCESS -> {
                    hideLoader()
                    setEpisodeInfo(it.data)
                }
                ERROR -> {
                    hideLoader()
                    val msg = errorMessage.getMessage(it.error)
                    showError(msg, this::loadEpisode)
                }
            }
        }
    }

    private fun loadCharacters() {
        episode?.let {
            hideError()
            viewModel.getCharacters(it.characters)
        }
    }

    private fun listenCharactersData() {
        viewModel.characters.observe(viewLifecycleOwner) {
            when (it.status) {
                LOADING -> showLoader()
                SUCCESS -> {
                    hideLoader()
                    setCharacterInfo(it.data ?: emptyList())
                }
                ERROR -> {
                    hideLoader()
                    val msg = errorMessage.getMessage(it.error)
                    showError(msg, this::loadCharacters)
                }
            }
        }
    }

    private fun showLoader() {
        binding.loader.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        binding.loader.visibility = View.GONE
    }

    private fun showError(msg: String, retry: () -> Unit) {
        binding.fatalError.root.visibility = View.VISIBLE
        binding.fatalError.fatalErrorMessage.text = msg
        binding.fatalError.fatalErrorRetry.setOnClickListener { retry() }
    }

    private fun hideError() {
        binding.fatalError.root.visibility = View.GONE
    }

    private fun setEpisodeInfo(episode: Episode?) {
        this.episode = episode
        episode?.let {
            binding.groupDetail.visibility = View.VISIBLE
            binding.name.text = it.name
            binding.date.text = it.airDate
            Glide.with(binding.episodeImage)
                .load(R.mipmap.rick)
                .into(binding.episodeImage)

            loadCharacters()
        }
    }

    private fun setCharacterInfo(data: List<Character>) {
        adapter.data = data
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
