package com.dariochamorro.presentation.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dariochamorro.domain.models.Character
import com.dariochamorro.presentation.R
import com.dariochamorro.presentation.databinding.ItemCharacterBinding

class CharacterAdapter : RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    var data: List<Character> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_character, parent, false)
        return CharacterViewHolder(view)
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    class CharacterViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        private val binding = ItemCharacterBinding.bind(view)

        fun bind(character: Character) {
            binding.chName.text = character.name
            binding.chSpecie.text = character.species
            binding.chStatus.text = character.status.name

            Glide
                .with(binding.chImage.context)
                .load(character.image)
                .into(binding.chImage)
        }
    }
}
