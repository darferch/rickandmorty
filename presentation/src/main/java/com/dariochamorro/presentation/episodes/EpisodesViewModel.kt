package com.dariochamorro.presentation.episodes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.domain.usecases.ListEpisodesUseCase
import com.dariochamorro.domain.usecases.LoadEpisodesUseCase
import com.dariochamorro.domain.util.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EpisodesViewModel @Inject constructor(
    listEpisodesUseCase: ListEpisodesUseCase,
    private val loadEpisodesUseCase: LoadEpisodesUseCase,
    private val dispatchers: DispatcherProvider
) : ViewModel() {

    private val loadEpisodeControl: MutableLiveData<AsyncResult<Unit>> = MutableLiveData()
    val loadEpisodeResult: LiveData<AsyncResult<Unit>> = loadEpisodeControl

    val episodes: LiveData<AsyncResult<List<Episode>>> = listEpisodesUseCase()
        .asLiveData()

    fun loadEpisodes() = viewModelScope.launch(dispatchers.main()) {
        loadEpisodeControl.value = AsyncResult.loading()
        loadEpisodeControl.value = loadEpisodesUseCase()
    }
}
