package com.dariochamorro.presentation.episodes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.dariochamorro.domain.common.AsyncResultStatus.ERROR
import com.dariochamorro.domain.common.AsyncResultStatus.LOADING
import com.dariochamorro.domain.common.AsyncResultStatus.SUCCESS
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.presentation.common.ErrorMessage
import com.dariochamorro.presentation.databinding.FragmentEpisodesBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class EpisodesFragment : Fragment() {

    @Inject
    lateinit var errorMessage: ErrorMessage
    private val viewModel: EpisodesViewModel by viewModels()
    private val adapter: EpisodesAdapter = EpisodesAdapter(this::goToDetail)
    private var _binding: FragmentEpisodesBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEpisodesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        listenData()
        listenDataState()
        loadEpisodes()
    }

    private fun setupRecycler() {
        binding.recycler.adapter = adapter
    }

    private fun listenData() {
        viewModel.episodes.observe(viewLifecycleOwner) {
            adapter.submitList(it.data)
        }
    }

    private fun listenDataState() {
        viewModel.loadEpisodeResult.observe(viewLifecycleOwner) {
            when (it.status) {
                SUCCESS -> {
                    hideLoader()
                }
                LOADING -> {
                    showLoader()
                }
                ERROR -> {
                    hideLoader()
                    showError(it.error)
                }
            }
        }
    }

    private fun loadEpisodes() {
        hideError()
        viewModel.loadEpisodes()
    }

    private fun showLoader() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showError(error: Errors?) {
        val message = errorMessage.getMessage(error)
        when (error) {
            Errors.NetworkError -> showMinorError(message)
            else -> showFatalError(message)
        }
    }

    private fun showMinorError(msg: String) {
        binding.minorError.root.visibility = View.VISIBLE
        binding.minorError.minorErrorMessage.text = msg
        binding.minorError.minorErrorRetry.setOnClickListener { loadEpisodes() }
    }

    private fun showFatalError(msg: String) {
        binding.fatalError.root.visibility = View.VISIBLE
        binding.fatalError.fatalErrorMessage.text = msg
        binding.fatalError.fatalErrorRetry.setOnClickListener { loadEpisodes() }
    }

    private fun hideError() {
        binding.fatalError.root.visibility = View.GONE
        binding.minorError.root.visibility = View.GONE
    }

    private fun goToDetail(episode: Episode) {
        val action = EpisodesFragmentDirections.actionEpisodesToDetail(episode.id)
        findNavController().navigate(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
