package com.dariochamorro.presentation.episodes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.presentation.R
import com.dariochamorro.presentation.databinding.ItemEpisodeBinding

class EpisodesAdapter(
    private val onClick: (Episode) -> Unit
) : ListAdapter<Episode, EpisodesAdapter.EpisodeViewHolder>(CommentDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_episode, parent, false)
        return EpisodeViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class EpisodeViewHolder(
        view: View,
        private val onClickEpisode: (Episode) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        private val binding = ItemEpisodeBinding.bind(view)
        private var currentEpisode: Episode? = null

        init {
            view.setOnClickListener {
                currentEpisode?.let(onClickEpisode)
            }
        }

        fun bind(episode: Episode) {
            currentEpisode = episode
            binding.episodeCode.text = episode.episode
            binding.episodeDate.text = episode.airDate
            binding.episodeName.text = episode.name
        }
    }
}

object CommentDiffCallback : DiffUtil.ItemCallback<Episode>() {
    override fun areItemsTheSame(oldItem: Episode, newItem: Episode): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Episode, newItem: Episode): Boolean {
        return oldItem == newItem
    }
}
