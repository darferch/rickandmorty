package com.dariochamorro.testing_commons.rules

import com.dariochamorro.domain.util.DispatcherProvider
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

@ExperimentalCoroutinesApi
class CoroutineRule : TestRule {

    private val dispatcher = TestCoroutineDispatcher()
    val testDispatchers = object : DispatcherProvider {
        override fun main(): CoroutineDispatcher = dispatcher

        override fun default(): CoroutineDispatcher = dispatcher

        override fun io(): CoroutineDispatcher = dispatcher

        override fun unconfined(): CoroutineDispatcher = dispatcher
    }

    override fun apply(base: Statement, description: Description): Statement =
        object : Statement() {
            override fun evaluate() {
                Dispatchers.setMain(dispatcher)
                base.evaluate()
                Dispatchers.resetMain()
                dispatcher.cleanupTestCoroutines()
            }
        }
}
