package com.dariochamorro.testing_commons.samples

abstract class DataSample<T> {

    abstract fun singleSample(index: Int = 1): T

    fun listSample(size: Int = 1): List<T> = (0..size).map(this::singleSample)
}
