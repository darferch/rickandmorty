package com.dariochamorro.testing_commons.samples

import com.dariochamorro.domain.models.Character
import com.dariochamorro.domain.models.CharacterStatus

class CharacterSample : DataSample<Character>() {
    override fun singleSample(index: Int): Character = Character(
        id = index,
        name = "Character $index",
        status = CharacterStatus.Unknown,
        species = "Specie $index",
        image = "image $index"
    )
}
