package com.dariochamorro.testing_commons.samples

import com.dariochamorro.domain.models.Character
import com.dariochamorro.domain.models.Episode
import java.lang.IllegalArgumentException
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
object SampleFactory {
    private val characterSample = CharacterSample()
    private val episodeSample = EpisodeSample()

    fun <T : Any> singleSample(kClass: KClass<T>): T = when (kClass) {
        Character::class -> characterSample.singleSample() as T
        Episode::class -> episodeSample.singleSample() as T
        else -> throw IllegalArgumentException("Class not have samples")
    }

    fun <T : Any> listSample(kClass: KClass<T>, size: Int = 1): List<T> = when (kClass) {
        Character::class -> characterSample.listSample(size) as List<T>
        Episode::class -> episodeSample.listSample(size) as List<T>
        else -> throw IllegalArgumentException("Class not have samples")
    }
}
