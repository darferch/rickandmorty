package com.dariochamorro.testing_commons.samples

import com.dariochamorro.domain.models.Episode

class EpisodeSample : DataSample<Episode>() {
    override fun singleSample(index: Int): Episode = Episode(
        id = index,
        name = "Episode $index",
        airDate = "March $index, 2021",
        episode = "S01E$index",
        characters = listOf(1, 2, 3)
    )
}
