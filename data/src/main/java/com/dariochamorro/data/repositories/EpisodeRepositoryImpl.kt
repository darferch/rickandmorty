package com.dariochamorro.data.repositories

import com.dariochamorro.data.datasources.EpisodeLocalDataSource
import com.dariochamorro.data.datasources.EpisodeRemoteDataSource
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.domain.respositories.EpisodeRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.io.IOException
import java.util.logging.Logger
import javax.inject.Inject

class EpisodeRepositoryImpl @Inject constructor(
    private val remoteDataSource: EpisodeRemoteDataSource,
    private val localDataSource: EpisodeLocalDataSource
) : EpisodeRepository {

    private val logger = Logger.getLogger(EpisodeRepositoryImpl::class.simpleName)

    override fun listEpisodes(): Flow<AsyncResult<List<Episode>>> {
        return localDataSource.listEpisodes()
            .map { AsyncResult.success(it) }
    }

    override suspend fun loadEpisodes(): AsyncResult<Unit> {
        return try {
            val episodes = remoteDataSource.getAllEpisodes()
            localDataSource.clearEpisodes()
            localDataSource.insertEpisodes(episodes)
            AsyncResult.success(Unit)
        } catch (e: IOException) {
            logger.warning(e.message)
            AsyncResult.error(Errors.NetworkError)
        } catch (e: Exception) {
            logger.warning(e.message)
            AsyncResult.error(Errors.UnknownError)
        }
    }

    override suspend fun getEpisodeById(episodeId: Int): AsyncResult<Episode> {
        return try {
            val episode = localDataSource.episodeById(episodeId)
            AsyncResult.success(episode)
        } catch (e: Exception) {
            logger.warning(e.message)
            AsyncResult.error(Errors.DatabaseError)
        }
    }
}
