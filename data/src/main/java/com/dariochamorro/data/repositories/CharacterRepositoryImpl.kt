package com.dariochamorro.data.repositories

import com.dariochamorro.data.datasources.CharacterRemoteDataSource
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.models.Character
import com.dariochamorro.domain.respositories.CharacterRepository
import java.io.IOException
import java.util.logging.Logger
import javax.inject.Inject

class CharacterRepositoryImpl @Inject constructor(
    private val characterRemoteDataSource: CharacterRemoteDataSource
) : CharacterRepository {

    private val logger = Logger.getLogger(CharacterRepositoryImpl::class.simpleName)

    override suspend fun characterByIds(ids: List<Int>): AsyncResult<List<Character>> {
        return try {
            val result = characterRemoteDataSource.getCharactersByIds(ids)
            AsyncResult.success(result)
        } catch (e: IOException) {
            logger.warning(e.message)
            AsyncResult.error(Errors.NetworkError)
        } catch (e: Exception) {
            logger.warning(e.message)
            AsyncResult.error(Errors.UnknownError)
        }
    }
}
