package com.dariochamorro.data.datasources

import com.dariochamorro.domain.models.Character

interface CharacterRemoteDataSource {

    suspend fun getCharactersByIds(ids: List<Int>): List<Character>
}
