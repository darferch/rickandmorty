package com.dariochamorro.data.datasources

import com.dariochamorro.domain.models.Episode

interface EpisodeRemoteDataSource {

    suspend fun getAllEpisodes(): List<Episode>
}
