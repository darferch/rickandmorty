package com.dariochamorro.data.datasources

import com.dariochamorro.domain.models.Episode
import kotlinx.coroutines.flow.Flow

interface EpisodeLocalDataSource {

    fun listEpisodes(): Flow<List<Episode>>

    suspend fun insertEpisodes(episodes: List<Episode>)

    suspend fun episodeById(episodeId: Int): Episode

    suspend fun clearEpisodes()
}
