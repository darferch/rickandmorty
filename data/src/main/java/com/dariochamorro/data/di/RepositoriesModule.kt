package com.dariochamorro.data.di

import com.dariochamorro.data.repositories.CharacterRepositoryImpl
import com.dariochamorro.data.repositories.EpisodeRepositoryImpl
import com.dariochamorro.domain.respositories.CharacterRepository
import com.dariochamorro.domain.respositories.EpisodeRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoriesModule {

    @Binds
    @Singleton
    abstract fun bindCharacterRepository(characterRepositoryImpl: CharacterRepositoryImpl): CharacterRepository

    @Binds
    @Singleton
    abstract fun bindEpisodeRepository(episodeRepositoryImpl: EpisodeRepositoryImpl): EpisodeRepository
}
