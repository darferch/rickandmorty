package com.dariochamorro.data

import com.dariochamorro.data.datasources.EpisodeLocalDataSource
import com.dariochamorro.data.datasources.EpisodeRemoteDataSource
import com.dariochamorro.data.repositories.EpisodeRepositoryImpl
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.atLeastOnce

@ExperimentalCoroutinesApi
class EpisodeRepositoryImplTest : CoroutineUnitTest() {

    @Mock
    lateinit var episodeRemoteDataSource: EpisodeRemoteDataSource

    @Mock
    lateinit var episodeLocalDataSource: EpisodeLocalDataSource
    lateinit var episodeRepositoryImpl: EpisodeRepositoryImpl

    @Before
    fun setup() {
        episodeRepositoryImpl =
            EpisodeRepositoryImpl(episodeRemoteDataSource, episodeLocalDataSource)
    }

    @Test
    fun `listEpisodes should emit a flow with a list of episodes`() = runBlockingTest {
        // Given
        val data = SampleFactory.listSample(Episode::class, 2)
        whenever(episodeLocalDataSource.listEpisodes()).thenReturn(listOf(data).asFlow())
        val dataExpected = AsyncResult.success(data)

        // When
        var result: AsyncResult<List<Episode>>? = null
        episodeRepositoryImpl.listEpisodes().collect { result = it }

        // Then
        assertEquals(dataExpected, result)
    }

    @Test
    fun `loadEpisodes should emit a success result`() = runBlockingTest {
        // Given
        val resultExpected = AsyncResult.success(Unit)

        // When
        val result = episodeRepositoryImpl.loadEpisodes()

        // Then
        assertEquals(resultExpected, result)
    }

    @Test
    fun `loadEpisodes should call getAllEpisodes from remote data source`() = runBlockingTest {
        // When
        episodeRepositoryImpl.loadEpisodes()

        // Then
        verify(episodeRemoteDataSource, atLeastOnce()).getAllEpisodes()
    }

    @Test
    fun `loadEpisodes should clear and insert the episodes in the local store`() = runBlockingTest {
        // Given
        val data: List<Episode> = SampleFactory.listSample(Episode::class, 2)
        whenever(episodeRemoteDataSource.getAllEpisodes()).thenReturn(data)

        // When
        episodeRepositoryImpl.loadEpisodes()

        // Then
        inOrder(episodeLocalDataSource) {
            verify(episodeLocalDataSource, atLeastOnce()).clearEpisodes()
            verify(episodeLocalDataSource, atLeastOnce()).insertEpisodes(data)
        }
    }

    @Test
    fun `loadEpisodes should return an error result if remote data source fails`() =
        runBlockingTest {
            // Given
            whenever(episodeRemoteDataSource.getAllEpisodes()).thenThrow(IllegalStateException())
            val expectedResult = AsyncResult.error<List<Episode>>(Errors.UnknownError)

            // When
            val result = episodeRepositoryImpl.loadEpisodes()

            // Then
            assertEquals(expectedResult, result)
        }

    @Test
    fun `getEpisodeById should return an Episode by Id`() = runBlockingTest {
        // Given
        val data = SampleFactory.singleSample(Episode::class)
        whenever(episodeLocalDataSource.episodeById(data.id)).thenReturn(data)

        val expectedResult = AsyncResult.success(data)

        // When
        val result = episodeRepositoryImpl.getEpisodeById(data.id)

        // Then
        assertEquals(expectedResult, result)
    }

    @Test
    fun `getEpisodeById should return an error result if local data source fails`() =
        runBlockingTest {
            // Given
            val id = 1
            whenever(episodeLocalDataSource.episodeById(id)).thenThrow(IllegalStateException())
            val expectedResult = AsyncResult.error<Episode>(Errors.DatabaseError)

            // When
            val result = episodeRepositoryImpl.getEpisodeById(id)

            // Then
            assertEquals(expectedResult, result)
        }
}
