package com.dariochamorro.data

import com.dariochamorro.data.datasources.CharacterRemoteDataSource
import com.dariochamorro.data.repositories.CharacterRepositoryImpl
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.models.Character
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class CharacterRepositoryImplTest : CoroutineUnitTest() {

    @Mock
    lateinit var characterRemoteDataSource: CharacterRemoteDataSource
    lateinit var characterRepositoryImpl: CharacterRepositoryImpl

    @Before
    fun setup() {
        characterRepositoryImpl = CharacterRepositoryImpl(characterRemoteDataSource)
    }

    @Test
    fun `characterByIds should return a list of Characters `() = runBlockingTest {
        // Given
        val ids = listOf(1, 2, 3)
        val data = SampleFactory.listSample(Character::class, 3)
        whenever(characterRemoteDataSource.getCharactersByIds(ids)).thenReturn(data)

        val expectedResult = AsyncResult.success(data)

        // When
        val result = characterRepositoryImpl.characterByIds(ids)

        // Then
        assertEquals(expectedResult, result)
    }

    @Test
    fun `characterByIds should should return an error if getCharactersByIds fails`() =
        runBlockingTest {
            // Given
            whenever(characterRemoteDataSource.getCharactersByIds(any())).thenThrow(
                IllegalStateException()
            )
            val expectedResult = AsyncResult.error<Unit>(Errors.UnknownError)

            // When
            val result = characterRepositoryImpl.characterByIds(emptyList())

            // Then
            assertEquals(expectedResult, result)
        }
}
