package com.dariochamorro.rickandmorty.di

import com.dariochamorro.data.di.RepositoriesModule
import com.dariochamorro.domain.di.UtilDomainModule
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(
    includes = [
        RepositoriesModule::class,
        UtilDomainModule::class
    ]
)
@InstallIn(SingletonComponent::class)
class AppModule
