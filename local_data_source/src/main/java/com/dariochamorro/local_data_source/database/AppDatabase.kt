package com.dariochamorro.local_data_source.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dariochamorro.local_data_source.dao.EpisodeDao
import com.dariochamorro.local_data_source.entities.EpisodeEntity

@Database(version = 1, entities = [EpisodeEntity::class])
abstract class AppDatabase : RoomDatabase() {

    abstract fun episodeDao(): EpisodeDao
}
