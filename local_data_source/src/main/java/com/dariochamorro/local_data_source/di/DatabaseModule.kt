package com.dariochamorro.local_data_source.di

import com.dariochamorro.local_data_source.dao.EpisodeDao
import com.dariochamorro.local_data_source.database.DatabaseProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideEpisodeDao(databaseProvider: DatabaseProvider): EpisodeDao = databaseProvider
        .database
        .episodeDao()
}
