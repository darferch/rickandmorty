package com.dariochamorro.local_data_source.di

import com.dariochamorro.data.datasources.EpisodeLocalDataSource
import com.dariochamorro.local_data_source.EpisodeLocalDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class LocalDataSourceModule {

    @Binds
    @Singleton
    abstract fun bindEpisodeLocalDataSource(episodeLocalDataSourceImpl: EpisodeLocalDataSourceImpl): EpisodeLocalDataSource
}
