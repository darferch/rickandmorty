package com.dariochamorro.local_data_source.mappers

import com.dariochamorro.domain.common.ModelMapper
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.local_data_source.entities.EpisodeEntity
import javax.inject.Inject

class EpisodeEntityMapper @Inject constructor() : ModelMapper<Episode, EpisodeEntity>() {
    override fun toModel(data: EpisodeEntity): Episode = Episode(
        id = data.episodeId,
        name = data.name,
        airDate = data.airDate,
        episode = data.episode,
        characters = data.characters.split(",").map { it.toInt() }
    )

    override fun fromModel(model: Episode): EpisodeEntity = EpisodeEntity(
        episodeId = model.id,
        name = model.name,
        airDate = model.airDate,
        episode = model.episode,
        characters = model.characters.joinToString(",")
    )
}
