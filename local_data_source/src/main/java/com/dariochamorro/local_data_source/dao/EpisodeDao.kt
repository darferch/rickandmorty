package com.dariochamorro.local_data_source.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.dariochamorro.local_data_source.entities.EpisodeEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface EpisodeDao {

    @Insert
    suspend fun insertMany(episodes: List<EpisodeEntity>)

    @Query("SELECT * FROM episodes")
    fun listAll(): Flow<List<EpisodeEntity>>

    @Query("SELECT * FROM episodes WHERE episodeId = :episodeId")
    suspend fun getEpisodeById(episodeId: Int): EpisodeEntity

    @Query("DELETE FROM episodes")
    suspend fun clear()
}
