package com.dariochamorro.local_data_source

import com.dariochamorro.data.datasources.EpisodeLocalDataSource
import com.dariochamorro.domain.models.Episode
import com.dariochamorro.local_data_source.dao.EpisodeDao
import com.dariochamorro.local_data_source.mappers.EpisodeEntityMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class EpisodeLocalDataSourceImpl @Inject constructor(
    private val dao: EpisodeDao,
    private val mapper: EpisodeEntityMapper
) : EpisodeLocalDataSource {

    override fun listEpisodes(): Flow<List<Episode>> = dao.listAll()
        .map(mapper::toListModel)

    override suspend fun insertEpisodes(episodes: List<Episode>) {
        val data = mapper.fromListModel(episodes)
        dao.insertMany(data)
    }

    override suspend fun episodeById(episodeId: Int): Episode {
        val data = dao.getEpisodeById(episodeId)
        return mapper.toModel(data)
    }

    override suspend fun clearEpisodes() {
        dao.clear()
    }
}
