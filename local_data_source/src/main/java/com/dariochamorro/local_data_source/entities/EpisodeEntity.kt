package com.dariochamorro.local_data_source.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "episodes")
class EpisodeEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    val episodeId: Int = 0,
    val name: String = "",
    val airDate: String = "",
    val episode: String = "",
    val characters: String = ""
)
