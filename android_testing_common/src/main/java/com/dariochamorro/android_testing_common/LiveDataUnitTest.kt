package com.dariochamorro.android_testing_common

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dariochamorro.testing_commons.CoroutineUnitTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule

@ExperimentalCoroutinesApi
open class LiveDataUnitTest : CoroutineUnitTest() {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

}